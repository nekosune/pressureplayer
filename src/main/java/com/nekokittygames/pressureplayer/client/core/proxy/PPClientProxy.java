package com.nekokittygames.pressureplayer.client.core.proxy;

import com.nekokittygames.pressureplayer.common.Items.ModItems;
import com.nekokittygames.pressureplayer.common.blocks.ModBlocks;
import com.nekokittygames.pressureplayer.common.core.misc.Configs;
import com.nekokittygames.pressureplayer.common.core.proxy.PPCommonProxy;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * Created by nekosune on 21/09/14.
 */
public class PPClientProxy extends PPCommonProxy {

    public static boolean curStatus=false;
    @Override
    public boolean ShowBlock() {
        if(Minecraft.getMinecraft().thePlayer==null)
            return false;
        if(Minecraft.getMinecraft().thePlayer.getCurrentEquippedItem()==null) {
            if(curStatus!=false)
            {
                EntityPlayer player=Minecraft.getMinecraft().thePlayer;

                int rad = 20; // update radius
                Minecraft.getMinecraft().theWorld.markBlockRangeForRenderUpdate((int) player.posX - rad, (int) player.posY - rad, (int) player.posZ - rad, (int) player.posX + rad, (int) player.posY + rad, (int) player.posZ + rad);
                curStatus=false;
            }
            return false;
        }
        if(Minecraft.getMinecraft().thePlayer.getCurrentEquippedItem().itemID== ModItems.SecretToolItem.itemID || Minecraft.getMinecraft().thePlayer.getCurrentEquippedItem().itemID==new ItemStack(ModBlocks.player).itemID ) {
            if(curStatus!=true)
            {
                EntityPlayer player=Minecraft.getMinecraft().thePlayer;

                int rad = 20; // update radius
                Minecraft.getMinecraft().theWorld.markBlockRangeForRenderUpdate((int) player.posX - rad, (int) player.posY - rad, (int) player.posZ - rad, (int) player.posX + rad, (int) player.posY + rad, (int) player.posZ + rad);
                curStatus=true;
            }
            return true;
        }
        else
        {
            if(curStatus!=false)
            {
                EntityPlayer player=Minecraft.getMinecraft().thePlayer;

                int rad = 20; // update radius
                Minecraft.getMinecraft().theWorld.markBlockRangeForRenderUpdate((int) player.posX - rad, (int) player.posY - rad, (int) player.posZ - rad, (int) player.posX + rad, (int) player.posY + rad, (int) player.posZ + rad);
                curStatus=false;
            }
            return false;
        }
    }
}
