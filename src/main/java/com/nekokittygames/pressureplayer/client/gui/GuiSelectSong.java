package com.nekokittygames.pressureplayer.client.gui;

import com.nekokittygames.pressureplayer.common.blocks.tiles.tilePlayerBlock;
import com.nekokittygames.pressureplayer.common.network.PacketManager;
import com.nekokittygames.pressureplayer.common.network.packets.PacketSetSong;
import cpw.mods.fml.common.network.PacketDispatcher;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.texture.TextureObject;
import net.minecraft.client.resources.I18n;
import net.minecraft.tileentity.TileEntityCommandBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.ISaveFormat;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

/**
 * Created by nekosune on 23/09/14.
 */
public class GuiSelectSong extends GuiScreen {
    public static final int GUI_ID=25;
    public final int xSizeOfTexture = 220;
    public final int ySizeOfTexture = 182;
    private GuiTextField theGuiTextField;
    private final tilePlayerBlock playerBlock;

    public GuiSelectSong(tilePlayerBlock playerBlock) {
        this.playerBlock = playerBlock;
    }

    @Override
    public void initGui() {
        Keyboard.enableRepeatEvents(true);
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 96 + 12, I18n.getString("pressurePlayer.setSong")));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 120 + 12, I18n.getString("gui.cancel")));
        this.theGuiTextField = new GuiTextField(this.fontRenderer, this.width / 2 - 100, 60, 200, 20);
        this.theGuiTextField.setFocused(true);
        this.theGuiTextField.setText(playerBlock.SoundFile);
        ((GuiButton)this.buttonList.get(0)).enabled = this.theGuiTextField.getText().trim().length() > 0;
    }

    @Override
    public void drawScreen(int i, int i2, float v) {
        this.drawDefaultBackground();
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.renderEngine.bindTexture(new ResourceLocation("pressureplayer","textures/gui/guiSelectSound.png"));

        int posX = (this.width - xSizeOfTexture) / 2;
        int posY = (this.height - ySizeOfTexture) / 2;

        drawTexturedModalRect(posX, posY, 0, 0, xSizeOfTexture, ySizeOfTexture);
        this.theGuiTextField.drawTextBox();
        super.drawScreen(i, i2, v);
    }
    @Override
    public void onGuiClosed()
    {
        Keyboard.enableRepeatEvents(false);
    }

    @Override
    protected void keyTyped(char par1, int par2)
    {
        this.theGuiTextField.textboxKeyTyped(par1, par2);
        ((GuiButton)this.buttonList.get(0)).enabled = this.theGuiTextField.getText().trim().length() > 0;

        if (par2 == 28 || par2 == 156)
        {
            this.actionPerformed((GuiButton)this.buttonList.get(0));
        }
        if (par2 == 1) {
            this.actionPerformed((GuiButton)this.buttonList.get(1));
        }
    }

    /**
     * Called when the mouse is clicked.
     */
    @Override
    protected void mouseClicked(int par1, int par2, int par3)
    {
        super.mouseClicked(par1, par2, par3);
        this.theGuiTextField.mouseClicked(par1, par2, par3);
    }

    protected void actionPerformed(GuiButton par1GuiButton)
    {
        if (par1GuiButton.id == 1)
        {
            //close
            this.mc.displayGuiScreen((GuiScreen)null);
            this.mc.setIngameFocus();
            return;
        }
        if (par1GuiButton.enabled)
        {

            if (par1GuiButton.id == 0)
            {
                playerBlock.SoundFile=this.theGuiTextField.getText();

                PacketDispatcher.sendPacketToServer(PacketManager.buildPacket(new PacketSetSong(playerBlock,this.theGuiTextField.getText())));
                this.mc.displayGuiScreen((GuiScreen)null);
                this.mc.setIngameFocus();
                return;
            }
        }
    }
}
