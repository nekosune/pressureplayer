package com.nekokittygames.pressureplayer.client.sound;

import com.nekokittygames.pressureplayer.PressurePlayer;
import com.nekokittygames.pressureplayer.common.libs.LibIds;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.event.ForgeSubscribe;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by nekosune on 22/09/14.
 */
public class SoundManager {

    File[] SoundFiles;
    public void init()
    {
        Enumeration<URL> en= null;
        File[] filenames = new File[0];
        try {
            en = getClass().getClassLoader().getResources("assets/pressureplayer/sound");
            if (en.hasMoreElements()) {
                URL metaInf=en.nextElement();
                File fileMetaInf=new File(metaInf.toURI());
                filenames=fileMetaInf.listFiles();
            }
        } catch (IOException e) {
            PressurePlayer.log.severe("Failed to Register sounds:");
            PressurePlayer.log.throwing(this.getClass().getName(),"init",e);

        } catch (URISyntaxException e) {
            PressurePlayer.log.severe("Failed to Register sounds:");
            PressurePlayer.log.throwing(this.getClass().getName(), "init", e);
        }
        SoundFiles=filenames;


    }
    public List<String> getNames()
    {
        List<String> value=new ArrayList<String>();
        for(File file:SoundFiles)
        {
            value.add(file.getName().substring(0, file.getName().lastIndexOf('.')));
        }

        return value;
    }

    @SideOnly(Side.CLIENT)
    @ForgeSubscribe
    public void onSoundLoad(SoundLoadEvent event) {

        for(File sound : SoundFiles){

            try {
                String name=sound.getName();
                event.manager.soundPoolSounds.addSound(LibIds.ModId+":"+name);
            } catch(Exception e) {
                PressurePlayer.log.log(Level.WARNING, "Failed loading sound: " + sound);
            }
        }
    }



    @SideOnly(Side.CLIENT)
    public void PlaySound(String sound)
    {
        FMLClientHandler.instance().getClient().sndManager.sndSystem.fadeOutIn(sound,null,1000,1000);
    }

}
