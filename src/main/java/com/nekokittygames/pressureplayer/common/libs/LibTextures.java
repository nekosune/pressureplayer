package com.nekokittygames.pressureplayer.common.libs;

/**
 * Created by nekosune on 21/09/14.
 */
public class LibTextures {
    public static final String TEXTURE_BLOCK_CLEAR = LibIds.ModId + ":clear";
    public static final String TEXTURE_BLOCK_SOLID_AIR = LibIds.ModId + ":solid";
}
