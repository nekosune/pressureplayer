package com.nekokittygames.pressureplayer.common.libs;

/**
 * Created by nekosune on 21/09/14.
 */
public class LibIds {
    public static final String ModId="pressureplayer";
    public static final String ModName="Pressure Player";
    public static final String ModVersion="${version}";
    public static final String NETWORK_CHANNEL = ModId;
    public static final String COMMON_PROXY = "com.nekokittygames.pressureplayer.common.core.proxy.PPCommonProxy";
    public static final String CLIENT_PROXY = "com.nekokittygames.pressureplayer.client.core.proxy.PPClientProxy";
}
