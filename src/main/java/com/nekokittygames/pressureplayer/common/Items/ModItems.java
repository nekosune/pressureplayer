package com.nekokittygames.pressureplayer.common.Items;

import com.nekokittygames.pressureplayer.common.core.misc.Configs;
import com.nekokittygames.pressureplayer.common.core.misc.PPCreativeTab;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.Item;

/**
 * Created by nekosune on 21/09/14.
 */
public class ModItems {



    public static Item SecretToolItem;


    public static void InitItems()
    {
        SecretToolItem=new SecretToolItem(Configs.secretToolID).setUnlocalizedName("SecretTool").setCreativeTab(PPCreativeTab.INSTANCE);

        RegisterItems();
    }

    public static void RegisterItems()
    {
        GameRegistry.registerItem(SecretToolItem,"SecretTool");
    }
}
