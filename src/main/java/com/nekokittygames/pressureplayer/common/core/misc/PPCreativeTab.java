package com.nekokittygames.pressureplayer.common.core.misc;

import net.minecraft.creativetab.CreativeTabs;

/**
 * Created by nekosune on 21/09/14.
 */
public class PPCreativeTab extends CreativeTabs {

    public static PPCreativeTab INSTANCE;
    public PPCreativeTab() {
        super("Pressure Player");
    }

}
