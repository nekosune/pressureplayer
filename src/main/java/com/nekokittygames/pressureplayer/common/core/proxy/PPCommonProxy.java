package com.nekokittygames.pressureplayer.common.core.proxy;

import com.nekokittygames.pressureplayer.client.gui.GuiSelectSong;
import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

/**
 * Created by nekosune on 21/09/14.
 */
public class PPCommonProxy implements IGuiHandler{

    public boolean ShowBlock()
    {
        return true;
    }

    @Override
    public Object getServerGuiElement(int i, EntityPlayer entityPlayer, World world, int i2, int i3, int i4) {
        return null;
    }

    @Override
    public Object getClientGuiElement(int i, EntityPlayer entityPlayer, World world, int i2, int i3, int i4) {
        if(i== GuiSelectSong.GUI_ID)
            return new GuiSelectSong((com.nekokittygames.pressureplayer.common.blocks.tiles.tilePlayerBlock) world.getBlockTileEntity(i2,i3,i4));
        return null;
    }
}
