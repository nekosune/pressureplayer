package com.nekokittygames.pressureplayer.common.blocks.tiles;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet132TileEntityData;
import net.minecraft.tileentity.TileEntity;

/**
 * Created by nekosune on 22/09/14.
 */
public class tilePlayerBlock extends TileEntity {

    public String SoundFile="";

    @Override
    public void readFromNBT(NBTTagCompound par1NBTTagCompound) {
        super.readFromNBT(par1NBTTagCompound);
        readCustomNBT(par1NBTTagCompound);
    }

    @Override
    public void writeToNBT(NBTTagCompound par1NBTTagCompound) {
        super.writeToNBT(par1NBTTagCompound);
        writeCustomNBT(par1NBTTagCompound);
    }

    public void readCustomNBT(NBTTagCompound nbt)
    {
        SoundFile=nbt.getString("Sound");
    }

    public void writeCustomNBT(NBTTagCompound nbt)
    {
        nbt.setString("Sound",SoundFile);

    }

    @Override
    public void onDataPacket(INetworkManager net, Packet132TileEntityData pkt) {
        super.onDataPacket(net, pkt);
        readCustomNBT(pkt.data);
    }

    @Override
    public Packet getDescriptionPacket() {
        NBTTagCompound nbttagcompound = new NBTTagCompound();
        writeCustomNBT(nbttagcompound);
        return new Packet132TileEntityData(xCoord, yCoord, zCoord, -999, nbttagcompound);
    }
}
