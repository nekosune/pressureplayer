package com.nekokittygames.pressureplayer.common.blocks;

import com.nekokittygames.pressureplayer.common.blocks.tiles.tilePlayerBlock;
import com.nekokittygames.pressureplayer.common.core.misc.Configs;
import com.nekokittygames.pressureplayer.common.core.misc.PPCreativeTab;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;

/**
 * Created by nekosune on 21/09/14.
 */
public class ModBlocks {


    public static Block player;




    public static void InitBlocks()
    {
        player=new PlayerBlock(Configs.playerBlockId).setBlockUnbreakable().setUnlocalizedName("pressureplayer.player").setCreativeTab(PPCreativeTab.INSTANCE);

        RegisterBlocks();
    }

    public static void RegisterBlocks()
    {
        GameRegistry.registerBlock(player,"player");
        RegisterTileEntities();
    }


    public static void RegisterTileEntities()
    {
        GameRegistry.registerTileEntity(tilePlayerBlock.class,"tilePlayerBlock");
    }

}
