package com.nekokittygames.pressureplayer.common.blocks;

import com.nekokittygames.pressureplayer.PressurePlayer;
import com.nekokittygames.pressureplayer.client.gui.GuiSelectSong;
import com.nekokittygames.pressureplayer.client.sound.SoundManager;
import com.nekokittygames.pressureplayer.common.Items.ModItems;
import com.nekokittygames.pressureplayer.common.blocks.tiles.tilePlayerBlock;
import com.nekokittygames.pressureplayer.common.libs.LibTextures;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.ForgeDirection;

import java.util.List;
import java.util.Random;

/**
 * Created by nekosune on 21/09/14.
 */
public class PlayerBlock extends BlockContainer {


    private Icon clear;
    public PlayerBlock(int par1) {
        super(par1, Material.iron);
        this.setLightOpacity(0);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister) {
        blockIcon = par1IconRegister.registerIcon(LibTextures.TEXTURE_BLOCK_SOLID_AIR);
        clear = par1IconRegister.registerIcon(LibTextures.TEXTURE_BLOCK_CLEAR);
    }

    @Override
    @SideOnly(value = Side.CLIENT)
    public Icon getIcon(int par1, int par2) {
        if (!PressurePlayer.proxy.ShowBlock())
            return clear;
        else
            return blockIcon;
    }


    @Override
    public boolean getBlocksMovement(IBlockAccess par1iBlockAccess, int par2, int par3, int par4)
    {
        return false;
    }

    @Override
    public void onEntityCollidedWithBlock(World par1World, int par2, int par3, int par4, Entity par5Entity) {
        super.onEntityCollidedWithBlock(par1World, par2, par3, par4, par5Entity);
        if(par1World.isRemote)
        {

        }
    }

    @Override
    public boolean isOpaqueCube()
    {
        return PressurePlayer.proxy.ShowBlock();
    }
    @Override
    public boolean isCollidable()
    {
        return PressurePlayer.proxy.ShowBlock();
    }
    @Override
    @SideOnly(Side.CLIENT)
    public int getRenderColor(int par1)
    {
        return 0xffffff;
    }
    @Override
    public int getMobilityFlag()
    {
        return 0;
    }

    @Override
    public boolean isBlockNormalCube(World world, int x, int y, int z) {
        return PressurePlayer.proxy.ShowBlock();
    }



    @Override
    public boolean isBlockSolid(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5) {
        return PressurePlayer.proxy.ShowBlock();
    }


    @Override
    public boolean canPlaceTorchOnTop(World world, int x, int y, int z)
    {
        return PressurePlayer.proxy.ShowBlock();
    }

    @Override
    public boolean canCollideCheck(int par1, boolean par2) {
        return PressurePlayer.proxy.ShowBlock();
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4) {
        return null;
    }
    @Override
    public boolean renderAsNormalBlock()
    {
        return false;
    }

    @Override
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9) {
        if(par5EntityPlayer.getCurrentEquippedItem().itemID== ModItems.SecretToolItem.itemID)
        {
            List<String> temp= PressurePlayer.soundManager.getNames();
            par5EntityPlayer.openGui(PressurePlayer.instance, GuiSelectSong.GUI_ID,par1World,par2,par3,par4);
            return true;
        }
        return false;
    }

    @Override
    public TileEntity createNewTileEntity(World world) {
        return new tilePlayerBlock();
    }
}
