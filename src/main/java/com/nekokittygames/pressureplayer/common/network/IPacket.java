package com.nekokittygames.pressureplayer.common.network;

import cpw.mods.fml.common.network.Player;
import net.minecraft.network.INetworkManager;

import java.io.Serializable;

/**
 * Created by nekosune on 01/10/14.
 */
public interface IPacket extends Serializable {
    public void handle(INetworkManager manager, Player player);
}
