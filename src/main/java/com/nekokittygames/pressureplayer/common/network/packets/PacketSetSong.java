package com.nekokittygames.pressureplayer.common.network.packets;

import com.nekokittygames.pressureplayer.common.blocks.tiles.tilePlayerBlock;
import cpw.mods.fml.common.network.PacketDispatcher;

/**
 * Created by nekosune on 01/10/14.
 */
public class PacketSetSong extends PacketTile<tilePlayerBlock> {
    private static final long serialVersionUID = -2182522429849764376L;
    String song;
    public PacketSetSong(tilePlayerBlock tile,String songName) {
        super(tile);
        song=songName;
    }

    @Override
    public void handle() {
        tile.SoundFile=song;
        tile.worldObj.markBlockForUpdate(tile.xCoord,tile.yCoord,tile.zCoord);
        tile.worldObj.markBlockForUpdate();
    }
}
