package com.nekokittygames.pressureplayer.common.network.packets;

import com.nekokittygames.pressureplayer.PressurePlayer;
import com.nekokittygames.pressureplayer.common.network.IPacket;
import cpw.mods.fml.common.network.Player;
import net.minecraft.network.INetworkManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

/**
 * Created by nekosune on 01/10/14.
 */
public abstract class PacketTile<T extends TileEntity> implements IPacket {

    private static final long serialVersionUID = -1447633008013055477L;
    protected int dim, x, y, z;
    protected transient T tile;
    protected transient Player player;
    public PacketTile(T tile) {
        this.tile = tile;
        this.x = tile.xCoord;
        this.y = tile.yCoord;
        this.z = tile.zCoord;
        this.dim = tile.worldObj.provider.dimensionId;
    }
    @Override
    public void handle(INetworkManager manager, Player player) {
        MinecraftServer server = MinecraftServer.getServer();
        this.player = player;
        if(server != null) {
            World world = server.worldServerForDimension(dim);
            if(world == null) {

                PressurePlayer.log.severe("No world found for dimension " + dim + "!");
                return;
            }
            TileEntity tile = world.getBlockTileEntity(x, y, z);
            if(tile != null) {
                T castedTile = (T) tile;
                this.tile = castedTile;
                handle();
            }
        }
    }
    public abstract void handle();
}
