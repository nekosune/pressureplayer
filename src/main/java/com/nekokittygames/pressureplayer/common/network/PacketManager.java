package com.nekokittygames.pressureplayer.common.network;

import com.nekokittygames.pressureplayer.common.libs.LibIds;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;

import java.io.*;

/**
 * Created by nekosune on 01/10/14.
 */
public class PacketManager implements IPacketHandler {
    @Override
    public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player) {
        try {
            ByteArrayInputStream byteStream = new ByteArrayInputStream(packet.data);
            ObjectInputStream objStream = new ObjectInputStream(byteStream);
            IPacket ipacket = (IPacket) objStream.readObject();
            ipacket.handle(manager, player);
        } catch(Throwable e) {
            e.printStackTrace();
        }
    }

    public static Packet250CustomPayload buildPacket(IPacket ipacket) {
        try {
            Packet250CustomPayload payload = new Packet250CustomPayload();
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            ObjectOutputStream objStream = new ObjectOutputStream(byteStream);
            objStream.writeObject(ipacket);
            payload.channel = LibIds.NETWORK_CHANNEL;
            payload.data = byteStream.toByteArray();
            payload.length = payload.data.length;
            return payload;
        } catch(IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
