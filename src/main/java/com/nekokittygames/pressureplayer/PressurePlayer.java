package com.nekokittygames.pressureplayer;

import com.nekokittygames.pressureplayer.client.sound.SoundManager;
import com.nekokittygames.pressureplayer.common.Items.ModItems;
import com.nekokittygames.pressureplayer.common.blocks.ModBlocks;
import com.nekokittygames.pressureplayer.common.core.misc.Configs;
import com.nekokittygames.pressureplayer.common.core.misc.PPCreativeTab;
import com.nekokittygames.pressureplayer.common.core.proxy.PPCommonProxy;
import com.nekokittygames.pressureplayer.common.libs.LibIds;
import com.nekokittygames.pressureplayer.common.network.PacketManager;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;

import java.util.logging.Logger;

/**
 * Created by nekosune on 21/09/14.
 */
@Mod(modid = LibIds.ModId, version = LibIds.ModVersion,name=LibIds.ModName)
@NetworkMod(clientSideRequired = true,serverSideRequired = false,channels = { LibIds.NETWORK_CHANNEL }, packetHandler = PacketManager.class)
public class PressurePlayer {


    @Mod.Instance(LibIds.ModId)
    public static PressurePlayer instance;
    @SidedProxy(clientSide = LibIds.CLIENT_PROXY, serverSide = LibIds.COMMON_PROXY)
    public static PPCommonProxy proxy;


    public static Logger log;

    public static SoundManager soundManager;
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        log = event.getModLog();
        log.setParent(FMLLog.getLogger());
        Configuration config=new Configuration(event.getSuggestedConfigurationFile());
        Configs.load(config);
        PPCreativeTab.INSTANCE=new PPCreativeTab();
        soundManager=new SoundManager();
        soundManager.init();
        MinecraftForge.EVENT_BUS.register(soundManager);

    }

    @Mod.EventHandler
    public void load(FMLInitializationEvent event) {
        ModBlocks.InitBlocks();
        ModItems.InitItems();
        NetworkRegistry.instance().registerGuiHandler(instance,proxy);

    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        // Stub Method
    }
}
